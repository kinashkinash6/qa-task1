// idGenerator.js

function generateRandomID(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomID = '';
  
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      randomID += characters.charAt(randomIndex);
    }
  
    return randomID;
  }
  
  // Usage example:
  const randomID = generateRandomID(8); // Generate an 8-character random ID
  console.log(randomID); // Output: e.g., "45jhgdkr"
  
  