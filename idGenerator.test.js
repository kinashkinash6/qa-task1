// idGenerator.test.js

const generateID = require('./idGenerator');

describe('generateID', () => {
  it('should generate a non-empty ID', () => {
    const id = generateID();
    expect(id).toBeTruthy(); // Check if the generated ID is truthy 
  });

  it('should generate a unique ID', () => {
    const id1 = generateID();
    const id2 = generateID();
    expect(id1).not.toEqual(id2); // Check if two generated IDs are not equal
  });
});

